package org.sesame;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BanqueFinale implements CommandLineRunner {

	
	public static void main(String[] args) {
		
		SpringApplication.run(BanqueFinale.class, args);
		
	}

	@Override
	public void run(String... args) throws Exception {
	
	}

}
